# BrownieBot

Dumb Discord bot that I'm just writing cause I don't wanna use anyone else's

## config.json Template

Place this file in the data/ folder

```JSON
{
    "clientId": "YOUR ID GOES HERE",
    "token": "YOUR TOKEN GOES HERE",
    "prefix": "YOUR BOT PREFIX GOES HERE",
    "creatorId": YOUR DISCORD ID GOES HERE,
    "validId": [VALID IDs GO HERE, SEPERATE THEM WITH ,],
    "wolframAPIKey": "YOUR WOLFRAM API KEY GOES HERE"
}
```

## Other config stuff

For cogs.json, put the cogs you want the bot to load on startup, it starts with every cog in it

## browniebot.db - Database Schema

### allowed_roles
```
role_id INTEGER PRIMARY KEY | role_description TEXT | role_removable INTEGER
```

Used to determine which roles the bot is allowed to give/take from users.

### allowed_roles_user_count
```
role_id INTEGER PRIMARY KEY | user_count INTEGER | joined_count INTEGER | FOREIGN KEY(role_id) REFERENCES allowed_roles(role_id)
```

Used to limit how many people can get the given role. Resets daily.

### mod_log
```
user_id INTEGER PRIMARY KEY | mod_id INTEGER | mod_action TEXT | mod_reason TEXT | time INTEGER
```

Used to track what actions mods have taken.

### mod_values
```
mute_role_id INTEGER | mod_output_channel_id INTERGER
```

Used to hold assorted values which are needed when moderation actions are taken.

### muted_users
```
user_id INTEGER PRIMARY KEY | end_time INTEGER | is_self INTEGER
```

Used to hold the currently muted users.

### reaction_channels
```
channel_id INTEGER PRIMARY KEY | emoji BLOB
```

Reacts to images with the given emoji in the specified channels.

### reaction_check
```
guild_id INTEGER | channel_id INTEGER | message_id INTEGER PRIMARY KEY | role_emoji BLOB | role_id INTEGER | role_channel_id INTEGER | role_message TEXT
```

Watches for reactions of role_emoji on message_id. Gives the reacter the role and output the role_message in role_channel_id.

### reaction_data
```
user_id INTEGER | emoji BLOB | given_count INTEGER | received_count INTEGER | PRIMARY KEY (user_id, emoji)
```

Stores the reactions given and received by users.

### role_output
```
role_id INTEGER PRIMARY KEY | channel_id INTEGER | message TEXT | FOREIGN KEY(role_id) REFERENCES allowed_roles(role_id)
```

Used with allowed_roles. Makes it so the bot announces when a user is given the given role in the given channel with message.

### rules_roles
```
role_id INTEGER | rules_channel_id INTEGER | PRIMARY KEY (role_id, rules_channel_id)
```

Used to force a role to be the only obtainable role in the given channel.
