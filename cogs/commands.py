import random
import re
from pyfiglet import Figlet

import discord
from discord.ext import commands
from typing import Optional


class Commands(commands.Cog):
    def __init__(self, bot):
        self.roll_pattern = re.compile(r'([\d]+[d]{1}[\d]+)(\s*[+]\s*[\d]+[d]{1}[\d]+)*')
        self.roll_pattern_split = re.compile(r'\s*\+\s*')
        self.bot = bot
        self.clean_converter = bot.clean_converter.convert

    @commands.command()
    async def ping(self, ctx):
        """Pong!"""
        await ctx.send('pong!')

    @commands.command()
    async def echo(self, ctx, *, message: str = None):
        """Echoes a message"""
        # Checks to make sure a message was passed in
        if message:
            await ctx.send(await self.clean_converter(ctx, message))
        else:
            await ctx.send(f'```.echo [message]\n\nType your message '
                           f'and the bot will say it back.```')

    @commands.command()
    async def ninja(self, ctx, *, message: str = None):
        """Echoes a message, and then deletes the message that issued the command"""
        # Checks to make sure a message was passed in
        if message:
            await ctx.message.delete()
            await ctx.send(await self.clean_converter(ctx, message))
        else:
            await ctx.send.say(f'```.ninja [message]\n\nType your message '
                               f'and the bot will say it back, but not before '
                               f'deleting your message.```')

    @commands.command()
    async def figlet(self, ctx, *, message: str = None):
        """Echoes a message with figlet"""
        # Checks to make sure a message was passed in
        if message:
            await ctx.send(f"```{(Figlet().renderText(message))}```")
        else:
            await ctx.send(f'```.figlet [message]\n\nType your message '
                           f'and the bot will say it back as a figlet.```')

    @commands.command()
    async def roll(self, ctx, *, rolls: str = '1d6'):
        """Rolls dice using NdN format"""
        # If the user used the right format
        if re.fullmatch(self.roll_pattern, rolls):
            output = ''
            # For each dice type the user wants to roll
            for dice in re.split(self.roll_pattern_split, rolls):
                num_rolls, limit = map(int, dice.split('d'))
                # If more than 0 die were rolled
                if num_rolls > 0:
                    curr_sum = 0
                    output += f'D{limit}: '
                    # Rolling each dice
                    for _ in range(num_rolls):
                        curr_roll = random.randint(1, limit)
                        output += f'{curr_roll}, '
                        curr_sum += curr_roll
                    output = f'{output[:-2]} = {curr_sum}'
                    output += '\n'

            await ctx.send(output)
        else:
            await ctx.send(
                f'```.roll [NdN format]\n\n \
                Rolls dice, for multiple dice rolls use '+' as a seperator```')

    @commands.command()
    async def userinfo(self, ctx, *, user: Optional[discord.Member]):
        """Prints out info relating to the user that typed this message, or whoever was mentioned in this message"""
        if not user:
            user = ctx.author

        # Getting the status of the user
        if user.status:
            status = str(user.status).capitalize()
            if user.is_on_mobile():
                status += ' - Mobile'
        else:
            status = None

        # Getting the activity of the user
        if user.activity:
            if isinstance(user.activity, discord.Game):
                activity = f'Playing: {user.activity.name}'
            elif isinstance(user.activity, discord.Streaming):
                activity = f'Streaming: {user.activity.name}'
            elif isinstance(user.activity, discord.Spotify):
                activity = f'Listening to: {user.activity.title} by {user.activity.artist}'
            elif isinstance(user.activity, discord.Activity):
                activity = f'Doing something called: {user.activity.name}'
            elif isinstance(user.activity, discord.CustomActivity):
                emoji = user.activity.emoji 
                if emoji and emoji.is_custom_emoji():
                    emoji = f':{user.activity.emoji.name}: '
                elif emoji:
                    emoji = f'{user.activity.emoji.name} '
                else:
                    emoji = ''
                activity = f'{emoji}{user.activity.name if user.activity.name else ""}'
            else:
                activity = None
        else:
            activity = None

        # Getting the author's nickname and whether they are a bot
        author = (f'{user}'
                  f'{(("/" + str(user.nick)) if user.nick else "")}'
                  f'{("(Bot)" if user.bot else "")}')

        # Creating the embed
        data = discord.Embed(description=activity, colour=user.colour)
        data.set_author(name=author)
        data.set_thumbnail(url=user.avatar_url)
        data.add_field(name='Status', value=status, inline=False)
        days_since_joined = (user.joined_at.now()-user.joined_at).days
        data.add_field(
            name='Server Join Date',
            value=(f'{user.joined_at.strftime("%B %d, %Y, at %H:%M:%S")}\n'
                   f'({days_since_joined} days ago)'),
            inline=True)
        days_since_account_created = (
            user.created_at.now()-user.created_at).days
        data.add_field(
            name='Account Creation Date',
            value=(f'{user.created_at.strftime("%B %d, %Y, at %H:%M:%S")}\n'
                   f'({days_since_account_created} days ago)'),
            inline=True)
        # Getting the roles of the user
        roles = ', '.join(role.name for role in user.roles[1:])
        if roles:
            data.add_field(name='Roles', value=roles, inline=False)
        data.set_footer(text=f'User ID: {user.id}')
        await ctx.send(embed=data)

    @commands.command()
    async def serverinfo(self, ctx):
        """Prints out info relating to the server"""
        guild = ctx.guild

        data = discord.Embed(description=guild.description)
        data.set_author(name=guild.name)
        data.set_thumbnail(url=guild.icon_url)
        data.add_field(
            name='Server Creation Date',
            value=guild.created_at.strftime("%B %d, %Y, at %H:%M:%S"),
            inline=False)
        data.add_field(name='Members', value=len(guild.members), inline=False)
        data.add_field(name='Text Channels', value=len(
            guild.text_channels), inline=True)
        data.add_field(name='Voice Channels', value=len(
            guild.voice_channels), inline=True)
        owner = guild.get_member(guild.owner_id)
        data.set_footer(text=f'Owner: {owner} Server ID: {guild.id}')
        await ctx.send(embed=data)


def setup(bot):
    bot.add_cog(Commands(bot))
