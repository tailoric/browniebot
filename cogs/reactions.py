import asyncio
import json

import discord
from discord.ext import commands

# Database index legend
USER_ID = 0
EMOJI = 1
GIVEN_COUNT = 2
RECEIVED_COUNT = 3

# Time until reaction data gets committed to database
MINUTES = 5

class Reactions(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

        self.bot.loop.create_task(self.initialize_reaction_data())

        self.current_reactions = {}

        self.update_reaction_data_listener = self.bot.loop.create_task(
            self.update_data())

    def __exit__(self, exc_type, exc_value, traceback):
        """To cancel any running tasks"""
        self.update_reaction_data_listener.cancel()

    async def initialize_reaction_data(self):
        query = 'CREATE TABLE IF NOT EXISTS reaction_data (' \
                'user_id INTEGER, ' \
                'emoji BLOB, ' \
                'given_count INTEGER, ' \
                'received_count INTEGER,' \
                'PRIMARY KEY (user_id, emoji))'
        await self.bot.db.execute(query)
        await self.bot.db.commit()

    async def update_data(self):
        """Writing the reaction data every MINUTES minutes"""
        while True:
            await asyncio.sleep(MINUTES * 60)

            query = 'UPDATE reaction_data SET ' \
                        'given_count = given_count + ?, ' \
                        'received_count = received_count + ? ' \
                    'WHERE user_id = ? AND emoji = ?'

            for reaction_key in self.current_reactions:
                async with self.bot.db.execute(query, [self.current_reactions[reaction_key]['given_count'], self.current_reactions[reaction_key]['received_count'], reaction_key[USER_ID], reaction_key[EMOJI]]) as cursor:
                    if cursor.rowcount == 0:
                        query = 'INSERT INTO reaction_data VALUES(?, ?, ?, ?)'
                        await cursor.execute(query, [reaction_key[USER_ID], reaction_key[EMOJI],
                                                self.current_reactions[reaction_key]['given_count'],
                                                self.current_reactions[reaction_key]['received_count']])

            await self.bot.db.commit()

            self.current_reactions = {}

    @commands.Cog.listener()
    async def on_reaction_add(self, reaction, user):
        """Watching reactions by users"""
        # Cause bots aren't people
        if not user.bot and not reaction.message.author.bot:
            # If the emoji is a custom emoji
            emoji_used = reaction.emoji
            if isinstance(reaction.emoji, (discord.Emoji, discord.PartialEmoji)):
                emoji_used = ':' + emoji_used.name + ':'

            reaction_key = (user.id, emoji_used)
            if reaction_key in self.current_reactions:
                self.current_reactions[reaction_key]['given_count'] += 1
            else:
                self.current_reactions[reaction_key] = {}
                self.current_reactions[reaction_key]['given_count'] = 1
                self.current_reactions[reaction_key]['received_count'] = 0

            reaction_key = (reaction.message.author.id, emoji_used)
            if reaction_key in self.current_reactions:
                self.current_reactions[reaction_key]['received_count'] += 1
            else:
                self.current_reactions[reaction_key] = {}
                self.current_reactions[reaction_key]['given_count'] = 0
                self.current_reactions[reaction_key]['received_count'] = 1

    @commands.Cog.listener()
    async def on_reaction_remove(self, reaction, user):
        """Watching reactions by users"""
        # Cause bots aren't people
        if not user.bot and not reaction.message.author.bot:
            # If the emoji is a custom emoji
            emoji_used = reaction.emoji
            if isinstance(reaction.emoji, (discord.Emoji, discord.PartialEmoji)):
                emoji_used = ':' + emoji_used.name + ':'

            reaction_key = (user.id, emoji_used)
            if reaction_key in self.current_reactions:
                self.current_reactions[reaction_key]['given_count'] -= 1

            reaction_key = (reaction.message.author.id, emoji_used)
            if reaction_key in self.current_reactions:
                self.current_reactions[reaction_key]['received_count'] -= 1

    @commands.command()
    async def mystats(self, ctx):
        """Outputs the user's top reactions"""
        user = ctx.author
        data = discord.Embed(colour=user.colour)
        data.set_author(name=str(user))
        data.set_thumbnail(url=user.avatar_url)

        query = 'SELECT user_id, emoji, given_count, received_count FROM reaction_data WHERE user_id = ?'

        user_reaction_data = []

        async with self.bot.db.execute(query, [ctx.author.id]) as cursor:
            user_reaction_data = await cursor.fetchall()

        # Getting the user's top reactions
        user_reaction_data = sorted(
            user_reaction_data, key=lambda reaction: reaction[GIVEN_COUNT], reverse=True)

        received_reactions_data = ''
        for i in range(0, min(len(user_reaction_data), 10)):
            if user_reaction_data[i][GIVEN_COUNT] == 0:
                break
            received_reactions_data += f'{user_reaction_data[i][EMOJI]} - {user_reaction_data[i][GIVEN_COUNT]}, '

        received_reactions_data = received_reactions_data[:-2]

        if received_reactions_data:
            data.add_field(
                name='Top 10 received reactions:',
                value=(received_reactions_data),
                inline=False)

        # Getting the user's most received reactions
        user_reaction_data = sorted(
            user_reaction_data, key=lambda reaction: reaction[RECEIVED_COUNT], reverse=True)

        given_reactions_data = ''
        for i in range(0, min(len(user_reaction_data), 10)):
            if user_reaction_data[i][RECEIVED_COUNT] == 0:
                break
            given_reactions_data += f'{user_reaction_data[i][EMOJI]} - {user_reaction_data[i][RECEIVED_COUNT]}, '

        given_reactions_data = given_reactions_data[:-2]

        if given_reactions_data:
            data.add_field(
                name='Top 10 given reactions:',
                value=(given_reactions_data),
                inline=False)

        received_reactions_count = sum(x[RECEIVED_COUNT] for x in user_reaction_data)

        if received_reactions_count:
            data.add_field(
                name='Total reactions received:',
                value=(received_reactions_count),
                inline=True)

        given_reactions_count = sum(x[GIVEN_COUNT] for x in user_reaction_data)

        if given_reactions_count:
            data.add_field(
                name='Total reactions given:',
                value=(given_reactions_count),
                inline=True)

        await ctx.send(embed=data)

def setup(bot):
    bot.add_cog(Reactions(bot))
