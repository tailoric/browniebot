import inflect

import discord
from discord.ext import commands

# Database index legend
PRONOUNS_ROLE_ID = 0
PRONOUNS_PRONOUN = 1
PRONOUNS_PERSON = 2

# Pronoun marker role names
PRONOUNS_1ST_PERSON = "1st Person"
PRONOUNS_2ND_PERSON = "2nd Person"

class Pronouns(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.p = inflect.engine()

        # Creating the 'pronouns' table
        self.bot.loop.create_task(self.initialize_pronouns())

    async def initialize_pronouns(self):
        query = 'CREATE TABLE IF NOT EXISTS pronouns (' \
                'role_id INTEGER PRIMARY KEY, ' \
                'pronoun TEXT, ' \
                'person INTEGER)'
        await self.bot.db.execute(query)
        await self.bot.db.commit()

    @commands.group()
    async def pronouns(self, ctx):
        """Lets users give themselves pronouns"""
        if ctx.invoked_subcommand is None:
            await ctx.send_help(ctx.command)

    @pronouns.command()
    @commands.has_permissions(manage_roles=True)
    async def initialize(self, ctx):
        """Creates the markers for 1st and 2nd person pronouns"""
        if not any(role.name == PRONOUNS_1ST_PERSON for role in await ctx.guild.fetch_roles()):
            await ctx.guild.create_role(
                name=PRONOUNS_1ST_PERSON,
                permissions=discord.Permissions(0),
                colour=discord.Colour(0),
                hoist=False,
                mentionable=False
            )

        if not any(role.name == PRONOUNS_2ND_PERSON for role in ctx.guild.roles):
            await ctx.guild.create_role(
                name=PRONOUNS_2ND_PERSON,
                permissions=discord.Permissions(0),
                colour=discord.Colour(0),
                hoist=False,
                mentionable=False
            )

        await ctx.send('Pronoun markers have been initialized')

    @pronouns.command()
    async def list(self, ctx):
        """Lists all possible pronouns that can be assigned"""
        query = 'SELECT * FROM pronouns'
        cursor = await self.bot.db.execute(query)
        result = await cursor.fetchall()

        embed = discord.Embed(title='List of possible pronouns')
        for person in sorted(set([pronoun[PRONOUNS_PERSON] for pronoun in result])):
            for pronoun in [pronoun[PRONOUNS_PRONOUN] for pronoun in result if pronoun[PRONOUNS_PERSON] == person]:
                embed.add_field(name=pronoun, value=PRONOUNS_1ST_PERSON if person == 1 else PRONOUNS_2ND_PERSON)
        await ctx.send(embed=embed)

    @pronouns.command()
    @commands.has_permissions(manage_roles=True)
    async def create(self, ctx, pronoun: str = None, person: int = None):
        """Creates a pronoun"""
        if pronoun is None or person is None:
            await ctx.send_help(ctx.command)
            return

        clean_pronoun = await self.bot.clean_converter.convert(ctx, pronoun)

        # Checks if a role with the given name already exists
        if not any(role.name == pronoun for role in await ctx.guild.fetch_roles()):
            # Creates the pronoun
            new_pronoun = await ctx.guild.create_role(
                name=pronoun,
                permissions=discord.Permissions(0),
                colour=discord.Colour(0),
                hoist=False,
                mentionable=False
            )

            # Updating the roles to see their new positions
            roles = await ctx.guild.fetch_roles()
            person_name = PRONOUNS_1ST_PERSON if person == 1 else PRONOUNS_2ND_PERSON
            marker = next((role for role in roles if role.name == person_name), None)
            
            await new_pronoun.edit(position=marker.position)

            query = 'INSERT INTO pronouns VALUES (?, ?, ?)'
            await self.bot.db.execute(query, [new_pronoun.id, pronoun, person])
            await self.bot.db.commit()

            await ctx.send(f'Pronoun \'{clean_pronoun}\' successfully created')
        else:
            await ctx.send(f'Pronoun \'{clean_pronoun}\' already exists, or is a role')

    @pronouns.command()
    @commands.has_permissions(manage_roles=True)
    async def delete(self, ctx, pronoun: str = None):
        """Deletes the pronoun"""
        if pronoun is None:
            await ctx.send_help(ctx.command)
            return

        clean_pronoun = await self.bot.clean_converter.convert(ctx, pronoun)

        # Checks if a role with the given name already exists
        role = [role for role in await ctx.guild.roles if role.name.lower() == pronoun.lower()]
        if role:
            role = role[0]
            query = 'SELECT EXISTS(SELECT 1 FROM pronouns WHERE role_id = ?)'
            cursor = await self.bot.db.execute(query, [role.id])
            result = await cursor.fetchall()

            if result:
                query = 'DELETE FROM pronouns ' \
                        'WHERE role_id = ?'

                await self.bot.db.execute(query, [role.id])
                await self.bot.db.commit()
                await role.delete()

                await ctx.send(f'Pronoun \'{role.name}\' successfully deleted')
                return
        await ctx.send(f'Role \'{clean_pronoun}\' does not exist or is not a pronoun')

    async def assign_pronoun(self, ctx, pronoun):
        pass

    @pronouns.command()
    async def add(self, ctx, pronouns: str = None):
        """Assigns the pronouns"""
        pronoun_list = pronouns.split("/")

        query = 'SELECT * FROM pronouns'
        cursor = await self.bot.db.execute(query)
        result = await cursor.fetchall()

        pronoun_map = list(map(lambda x: x.lower(), pronoun_list))
        guild_map =  list(map(lambda x: x.name.lower(), await ctx.guild.fetch_roles()))
        pronoun_roles = [(ctx.guild.get_role(pronoun[PRONOUNS_ROLE_ID]), pronoun[PRONOUNS_PERSON]) for pronoun in result
                                    if pronoun[PRONOUNS_PRONOUN].lower() in pronoun_map and
                                        pronoun[PRONOUNS_PRONOUN].lower() in guild_map]

        if not pronoun_roles:
            await ctx.send(f'The pronouns you entered do not exist')

        author_map = list(map(lambda x: x.name.lower() , ctx.author.roles))
        assigned_pronouns = [pronoun[PRONOUNS_PERSON] for pronoun in result
                                if pronoun[PRONOUNS_PRONOUN].lower() in author_map]

        for (role, person) in pronoun_roles:
            person_name = PRONOUNS_1ST_PERSON if person == 1 else PRONOUNS_2ND_PERSON
            if person not in assigned_pronouns:
                await ctx.author.add_roles(role)
                await ctx.send(f'I\'ve given you the \'{role.name}\' {person_name} pronoun')
            else:
                await ctx.send(f'You already have a {person_name} pronoun')

    @pronouns.command()
    async def remove(self, ctx, pronouns: str = None):
        """Removes the pronouns"""
        pronoun_list = pronouns.split("/")

        query = 'SELECT * FROM pronouns'
        cursor = await self.bot.db.execute(query)
        result = await cursor.fetchall()

        pronoun_map = list(map(lambda x: x.lower(), pronoun_list))
        guild_map =  list(map(lambda x: x.name.lower(), await ctx.guild.fetch_roles()))
        pronoun_roles = [(ctx.guild.get_role(pronoun[PRONOUNS_ROLE_ID]), pronoun[PRONOUNS_PERSON]) for pronoun in result
                                    if pronoun[PRONOUNS_PRONOUN].lower() in pronoun_map and
                                        pronoun[PRONOUNS_PRONOUN].lower() in guild_map]

        for (role, person) in pronoun_roles:
            if role in ctx.author.roles:
                person_name = PRONOUNS_1ST_PERSON if person == 1 else PRONOUNS_2ND_PERSON
                try:
                    await ctx.author.remove_roles(role)
                    await ctx.send(f'I\'ve removed the \'{role.name}\' {person_name} pronoun from you')
                except Exception as e:
                    print(e)
                    await ctx.send(f'I can\'t remove the \'{role.name}\' pronoun from you')
            else:
                await ctx.send(f'You don\'t have the \'{role.name}\' pronoun')

def setup(bot):
    bot.add_cog(Pronouns(bot))
