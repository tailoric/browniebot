import discord
import asyncio
import wolframalpha
import util
from discord.ext import commands


def truncate(string, length):
    if string is None:
        return ''
    if len(string) > length - 2:
        return string[:length] + '..'
    return string


class Wolfram(commands.Cog):
    """WolframAlpha commands."""

    def __init__(self, bot):
        self.bot = bot
        # API key do not distribute
        self.wolfram = wolframalpha.Client(util.config['wolframAPIKey'])

    @commands.command()
    async def wolfram(self, ctx, *query_words: str):
        """Complex wolfram query with navigation of results."""

        # no arguments given
        if len(query_words) < 1:
            await ctx.send("No input")
            return

        # query as a single string
        query = " ".join(query_words)

        #
        response_message = await ctx.send('Query in progress...')
        response = self.wolfram.query(query)
        result = None

        # empty reponse, don't know what to do
        if not hasattr(response, 'pods'):
            await ctx.send("No response from wolframalpha")
            return

        # hack to avoid printing bot's IP address
        if 'IP address' in str(response):
            await response_message.edit_message(new_content="Response ignored for privacy concerns")
            return

        # prepare loop details
        embed = discord.Embed()
        current_pod = 1
        current_subpod = 0

        num_pods = len(response['pod'])

        # prepare reactions
        await response_message.add_reaction('⬅')    # arrow left (pods navigation)
        await response_message.add_reaction('➡')   # arrow right (pods navigation)
        await response_message.add_reaction('⬆')    # arrow up (subpods navigation)
        await response_message.add_reaction('⬇')    # arrow down (subpods navigation)

        author = ctx.message.author

        while True:
            result = response['pod'][current_pod]

            num_subpods = int(result['@numsubpods'])

            # format title
            if num_subpods > 1:
                embed.title = f"{result['@title']} ({current_pod + 1}/{num_pods}) [{current_subpod + 1}/{num_subpods}]"
            else:
                embed.title = f"{result['@title']} ({current_pod + 1}/{num_pods})"

            # format footer
            embed.set_footer(text=f"{result['@id']} | {response['@id']}")

            # determine content source
            subpod = None
            if num_subpods > 1:
                subpod = result['subpod'][current_subpod]
            elif num_subpods == 1:
                subpod = result['subpod']

            # embed image if there is one
            if 'img' in subpod:
                embed.set_image(url=subpod['img']['@src'])

            # set content
            # see https://discordapp.com/developers/docs/resources/channel#embed-limits for size limitations
            embed.description = truncate(subpod['plaintext'], 2000)

            await response_message.edit(
                new_content="Here are your results, navigate results with '⬅' and '➡', and navigate result pages with '⬆' and '⬇':",
                embed=embed)

            def check(r, u):
                return r.message.id == response_message.id and u == ctx.message.author

            try:
                reaction, user = await self.bot.wait_for('reaction_add', check=check, timeout=60.0)
            # user waited too long, halt
            except asyncio.TimeoutError:
                break

            # left arrow
            if str(reaction.emoji) == '⬅':
                current_pod = max(0, current_pod - 1)
                current_subpod = 0
            # right arrow
            if str(reaction.emoji) == '➡':
                current_pod = min(num_pods - 1, current_pod + 1)
                current_subpod = 0
            # up arrow
            if str(reaction.emoji) == '⬆':
                current_subpod = max(0, current_subpod - 1)
            # down arrow
            if str(reaction.emoji) == '⬇':
                current_subpod = min(num_subpods - 1, current_subpod + 1)

            await response_message.remove_reaction(str(reaction.emoji), author)

        await response_message.clear_reactions()
        await response_message.edit(new_content="Your response timed out")


def setup(bot):
    bot.add_cog(Wolfram(bot))
