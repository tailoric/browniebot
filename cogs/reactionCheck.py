from discord.ext import commands

# Database index legend
GUILD_ID = 0
CHANNEL_ID = 1
MESSAGE_ID = 2
ROLE_EMOJI = 3
ROLE_ID = 4
ROLE_CHANNEL_ID = 5
ROLE_MESSAGE = 6

class ReactionCheck(commands.Cog):
    # BUG: The functionality provided in this file did not work consistent, as the bot would miss on_raw_reaction_add events.
    #       This is probably due to the way the bot has previously been hosted, or may just be an issue with discord.

    # TODO: It should also make use of the roles cog to avoid repition of role related functionality.

    # NOTE: Currently, there is no use for this functionality, and so it will remain as is.

    def __init__(self, bot):
        self.bot = bot

        self.guild = None

        self.bot.loop.create_task(self.initialize_reaction_check())

    async def initialize_reaction_check(self):
        query = 'CREATE TABLE IF NOT EXISTS reaction_check (' \
                'guild_id INTEGER, ' \
                'channel_id INTEGER, ' \
                'message_id INTEGER PRIMARY KEY, ' \
                'role_emoji BLOB, ' \
                'role_id INTEGER, ' \
                'role_channel_id INTEGER, ' \
                'role_message TEXT)'
        await self.bot.db.execute(query)
        await self.bot.db.commit()

    async def grant_role(self, member):
        await member.add_roles(self.role)
        await self.role_channel.send(f'{self.reaction_check[ROLE_MESSAGE]} {member.display_name}!')

    async def prepare(self):
        self.reaction_check = None

        query = 'SELECT * FROM reaction_check'
        async with self.bot.db.execute(query) as cursor:
            self.reaction_check = await cursor.fetchall()

            # NOTE: This will only allow for one of these checks to be watched
            # NOFIX: Will not be fixed until the bugs with this functionality are fixed
            if self.reaction_check:
                self.reaction_check = self.reaction_check[0]

        if not self.reaction_check:
            # Resetting self.guild for when stopreactioncheck() is called
            self.guild = None
            return False

        # Initialize references
        self.guild = self.bot.get_guild(self.reaction_check[GUILD_ID])
        self.channel = self.bot.get_channel(self.reaction_check[CHANNEL_ID])
        self.message = await self.channel.fetch_message(self.reaction_check[MESSAGE_ID])
        self.role = self.guild.get_role(self.reaction_check[ROLE_ID])
        self.role_channel = self.guild.get_channel(self.reaction_check[ROLE_CHANNEL_ID])
        return True

    @commands.Cog.listener()
    async def on_member_join(self, member):
        if self.guild is None:
            found_reaction_check = await self.prepare()

            # If no message is being watched
            if not found_reaction_check:
                return

        # Search confirmation reaction
        reaction = next(
            (react for react in self.message.reactions if str(react.emoji) == self.reaction_check[ROLE_EMOJI]),
            None)

        # If reaction not found
        if reaction is None:
            return

        # Search for new member in existing confirmation reactions
        async for user in reaction.users():
            if user.id == member.id:
                await self.grant_role(member)
                return

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        """
            Watches for when a specific reaction is given to the watched message
            and gives the user the specified role if they do not already have it.
        """
        if self.guild is None:
            found_reaction_check = await self.prepare()

            # If no message is being watched
            if not found_reaction_check:
                return
        # Checks if the reaction was on the message we are watching
        if payload.message_id == self.message.id:
            # Checks if the emoji reacted is the one we are looking for
            if str(payload.emoji) == self.reaction_check[ROLE_EMOJI]:
                # Getting the member
                member = self.guild.get_member(payload.user_id)

                # If the member does not have the role
                if self.role not in member.roles:
                    # give role to member
                    await self.grant_role(member)

    async def error_helper(self, ctx, error_type: str, error_value: str):
        await ctx.send(f'The {error_type} given was not found. (Value: {error_value}')

    @commands.group()
    @commands.has_permissions(manage_guild=True)
    async def reactioncheck(self, ctx):
        """Watches a given message for a reaction, and then assigns a role if the reaction is given"""
        if ctx.invoked_subcommand is None:
            await ctx.send_help(ctx.command)

    @reactioncheck.command()
    async def start(self, ctx, channel_id: int = None, message_id: int = None,
                                        emoji = None , role_id: int = None,
                                        role_channel_id: int = None, role_message: str = None):
        """Starts watching for reactions on the given message id"""
        if role_message == None:
            await ctx.send(
                f'```.createreactioncheck [channel_id] [message_id] [emoji], [role_id], [role_channel_id], "[role_message]"\n\n'
                'Makes the bot watch for [emoji] reactions on the given message. When found, the bot will '
                'give the user the [role_id] role, and output [role_message] in the [role_channel_id] channel.\n'
                'You need to provide the [channel_id] and [message_id].```')
            return

        # Doing the actual stuff
        channel = self.bot.get_channel(channel_id)
        if not channel:
            await self.error_helper(ctx, "channel id", channel_id)
            return

        message = await channel.fetch_message(message_id)
        if not message:
            await self.error_helper(ctx, "message id", message_id)
            return

        role = channel.guild.get_role(role_id)
        if not role:
            await self.error_helper(ctx, "role id", role_id)
            return

        role_channel = self.bot.get_channel(role_channel_id)
        if not role_channel:
            await self.error_helper(ctx, "role channel id", role_channel_id)
            return

        query = 'INSERT INTO reaction_check ' \
                '(guild_id, channel_id, message_id, role_emoji, ' \
                    'role_id, role_channel_id, role_message) ' \
                'VALUES (?, ?, ?, ?, ?, ?, ?)'

        await self.bot.db.execute(query, [channel.guild.id, channel_id, message_id, emoji, role_id, role_channel_id, role_message])
        await self.bot.db.commit()

        await ctx.send('Now watching the given channel for reactions.')

    @reactioncheck.command()
    async def stop(self, ctx, message_id: int = None):
        """Stops watching for reactions on the given message id"""
        if message_id == None:
            await ctx.send('```.stopreactioncheck [message_id]\n\n'
                            'Stops watching the given message')
            return

        query = 'DELETE FROM reaction_check ' \
                'WHERE message_id = ?'

        output = await self.bot.db.execute(query, [message_id])
        await self.bot.db.commit()

        if output.rowcount:
            # This prepare will clear the set variables
            await self.prepare()
            await ctx.send(f'Stopped watching the message with id {message_id}')
        else:
            await ctx.send(f'The message with id {message_id} was not being watched')

def setup(bot):
    bot.add_cog(ReactionCheck(bot))
