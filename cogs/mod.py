import datetime
import typing

import discord
from discord.ext import commands, tasks
from discord.utils import get

# Database index legend
MOD_VALUES_MUTE_ROLE_ID = 0
MOD_VALUES_OUTPUT_MOD_CHANNEL_ID = 1

MOD_LOG_USER_ID = 0
MOD_LOG_MOD_ID = 1
MOD_LOG_MOD_ACTION = 2
MOD_LOG_MOD_REASON = 3
MOD_LOG_TIME = 4

MUTED_USERS_USER_ID = 0
MUTED_USERS_END_TIME = 1
MUTED_USERS_IS_SELF = 2


class Mod(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.mute_role = None
        self.mod_output_channel = None

        self.bot.loop.create_task(self.initialize_mod())

    async def initialize_mod(self):
        query = 'CREATE TABLE IF NOT EXISTS mod_values (' \
                'mute_role_id INTEGER, ' \
                'mod_output_channel_id INTERGER)'
        await self.bot.db.execute(query)

        query = 'CREATE TABLE IF NOT EXISTS mod_log (' \
                'user_id INTEGER, ' \
                'mod_id INTEGER, ' \
                'mod_action TEXT, ' \
                'mod_reason TEXT, ' \
                'time INTEGER)'
        await self.bot.db.execute(query)

        query = 'CREATE TABLE IF NOT EXISTS muted_users (' \
                'user_id INTEGER PRIMARY KEY, ' \
                'end_time INTEGER, ' \
                'is_self INTEGER)'
        await self.bot.db.execute(query)
        await self.bot.db.commit()

        self.check_mute.start()

    async def get_mute_role(self):
        """Gets the mute role so that the bot can remove it"""
        if self.mute_role != None:
            return True

        query = 'SELECT mute_role_id FROM mod_values'
        async with self.bot.db.execute(query) as cursor:
            #TODO: Make it possible to have multiple mute roles
            mute_role_id = await cursor.fetchall()
            for guild in self.bot.guilds:
                self.mute_role = get(guild.roles, id=mute_role_id[0][0])

                if not self.mute_role == None:
                    return True
        return False

    async def botLoad(self, ctx, extensionName: str):
        """Loads an extension"""
        try:
            self.bot.load_extension(f'cogs.{extensionName}')
        except Exception as error:
            await ctx.send(f'{type(error).__name__}\n{error}')
            return
        await ctx.send(f'{extensionName} loaded')

    async def botUnload(self, ctx, extensionName: str):
        """Unloads an extension"""
        try:
            self.bot.unload_extension(f'cogs.{extensionName}')
        except Exception as error:
            await ctx.send(f'{type(error).__name__}\n{error}')
            return
        await ctx.send(f'{extensionName} unloaded')

    async def botReload(self, ctx, extensionName: str):
        """Reloads an extension"""
        await self.botUnload(ctx, extensionName)
        await self.botLoad(ctx, extensionName)

    @commands.command()
    @commands.has_permissions(administrator=True)
    async def load(self, ctx, extensionName: str = None):
        """Command wrapper for botLoad"""
        if extensionName:
            await self.botLoad(ctx, extensionName)
        else:
            await ctx.send_help(ctx.command)

    @commands.command()
    @commands.has_permissions(administrator=True)
    async def unload(self, ctx, extensionName: str = None):
        """Command wrapper for botUnload"""
        if extensionName:
            # Makes it so you can not only unload this cog
            if (extensionName == 'mod'):
                await ctx.send('Please do not unload this')
                return
            await self.botUnload(ctx, extensionName)
        else:
            await ctx.send_help(ctx.command)

    @commands.command()
    @commands.has_permissions(administrator=True)
    async def reload(self, ctx, extensionName: str = None):
        """Command wrapper for botReload"""
        if extensionName:
            await self.botReload(ctx, extensionName)
        else:
            await ctx.send_help(ctx.command)

    async def add_to_mod_log(self, user, mod, mod_action, mod_reason):
        """Inserts the values into the 'mod_log' table"""
        query = 'INSERT INTO mod_log VALUES (?, ?, ?, ?, ?)'
        await self.bot.db.execute(query, [user.id, mod.id, mod_action, mod_reason, datetime.datetime.utcnow()])
        await self.bot.db.commit()

        if self.mod_output_channel == None:
            query = 'SELECT mod_output_channel_id FROM mod_values'
            async with self.bot.db.execute(query) as cursor:
                mod_output_channel_id = await cursor.fetchall()

            if mod_output_channel_id == [] or mod_output_channel_id[0][0] == None:
                return

            self.mod_output_channel = self.bot.get_channel(mod_output_channel_id[0][0])

        # Another check is done here to avoid much more code duplication
        if self.mod_output_channel != None:
            await self.mod_output_channel.send(f'{mod.mention} performed \'{mod_action}\' against {user.mention}\nReason: {mod_reason}')

    async def message_user(self, user, message):
        """Attempts to message the user with the given message"""
        try:
            await user.send(message)
        except (discord.HTTPException, discord.Forbidden):
            await print(f'Failed to send message to {user.display_name}')

    async def update_mod_values(self, query, arguments):
        """Used to ensure there is only ever 1 row in 'mod_values'"""
        check_query = 'SELECT * FROM mod_values'
        async with self.bot.db.execute(check_query) as cursor:
                mod_values = await cursor.fetchall()

        if len (mod_values) == 0:
            insert_query = 'INSERT INTO mod_values DEFAULT VALUES'
            await self.bot.db.execute(insert_query)

        await self.bot.db.execute(query, arguments)
        await self.bot.db.commit()

    @commands.command()
    @commands.has_permissions(administrator=True)
    async def unsetmodchannel(self, ctx):
        """Unsets the output channel for mod actions"""
        query = 'UPDATE mod_values SET mod_output_channel_id = \'\''
        await self.update_mod_values(query, [])

        await ctx.send('Mod output channel unset')

    @commands.command()
    @commands.has_permissions(administrator=True)
    async def setmodchannel(self, ctx, channel_id: int = None):
        """Sets the channel with the given id as the output channel for mod actions"""
        if channel_id == None:
            await ctx.send('Please enter a channel id')
            return

        channel = self.bot.get_channel(channel_id)

        if channel == None:
            await ctx.send('Please enter a valid channel id')
            return

        query = 'UPDATE mod_values SET mod_output_channel_id = ?'
        await self.update_mod_values(query, [channel.id])

        self.mod_output_channel = None

        await ctx.send('Mod output channel set or updated')

    @commands.command(name='modlog')
    @commands.has_permissions(administrator=True)
    async def view_mod_log(self, ctx):
        """Displays actions taken by the mods"""
        #TODO Add means to navigate the mod log
        query = 'SELECT * from mod_log LIMIT 20'
        async with self.bot.db.execute(query) as cursor:
            entries = await cursor.fetchall()

        output = '```'
        REMAINING_BACKTICS = 3
        for entry in entries:
            user = await self.bot.fetch_user(entry[MOD_LOG_USER_ID])
            mod = await self.bot.fetch_user(entry[MOD_LOG_MOD_ID])

            curr_line = f'Mod: {mod.display_name}, User: {user.display_name}, Action: {entry[MOD_LOG_MOD_ACTION]}, '\
                        f'Reason: {entry[MOD_LOG_MOD_REASON]}, Time: {entry[MOD_LOG_TIME]}\n'

            #TODO: Move this catch to a better place
            if len(output) + len(curr_line) + REMAINING_BACKTICS > 2000:
                await ctx.send(output + '```')
                output = '```'
            output += curr_line
        output += '```'

        await ctx.send(output)

    async def create_mod_log_entry_param_check(self, ctx, users, reason):
        """Used for parameter checks in create_mod_log_entry"""
        if reason == None:
            await ctx.send('Please enter the reason for the warning')
            return False

        if users == []:
            await ctx.send('Please include at least 1 user that the warning applies to')
            return False

        return True

    @commands.command(name='addwarning', aliases=['warn'])
    @commands.has_permissions(administrator=True)
    async def create_mod_log_entry(self, ctx, users: commands.Greedy[commands.MemberConverter] = [], *, reason: str = None):
        """Creates an entry in the mod_log table"""
        param_check = await self.create_mod_log_entry_param_check(ctx, users, reason)

        if param_check:
            for user in users:
                await self.add_to_mod_log(user, ctx.author, 'Warning', reason)
            await ctx.send('Warning successfully made')

    @commands.command(name='addwarningdm', aliases=['warndm'])
    @commands.has_permissions(administrator=True)
    async def create_mod_log_entry_message(self, ctx, users: commands.Greedy[commands.MemberConverter] = [], *, reason: str = None):
        """Creates an entry in the mod_log table, and dms the users why they were warned"""
        param_check = await self.create_mod_log_entry_param_check(ctx, users, reason)

        if param_check:
            for user in users:
                await self.add_to_mod_log(user, ctx.author, 'Warning', reason)
                await self.message_user(user, f'You have been warned for the following reason: {reason}')
            await ctx.send('Warning successfully made')

    @commands.command()
    @commands.has_permissions(manage_messages=True)
    async def delete(self, ctx, users: commands.Greedy[commands.MemberConverter] = [], numberOfMessages: str = 10):
        """Deletes [x] messages from the channel this command was typed in (Can only be 1-99)"""
        # Checks if a number was typed
        try:
            numberOfMessages = int(numberOfMessages)
        except ValueError:
            await ctx.send(f'Please type a number')
            return

        # Checks if the number is between 1 and 99
        if numberOfMessages < 0:
            await ctx.send(f'Please make sure your number is '
                            f'positive')
            return

        # Only 100 messages can be delete in bulk
        numberOfMessages = numberOfMessages if numberOfMessages <= 100 else 100

        # Getting the messages
        deletedMessages = []
        async for message in ctx.channel.history(limit=100):
            if users == [] or message.author in users:
                if numberOfMessages == 0:
                    break
                deletedMessages.append(message)
                numberOfMessages -= 1

        #TODO rewrite to actually delete messages and not search through the limit
        try:
            await ctx.channel.delete_messages(deletedMessages)
            await ctx.send(f'Deleted {len(deletedMessages)} messages')
        except discord.errors.HTTPException:
            #TODO: Replace with purge command
            await ctx.send('Can only bulk delete messages that are under 14 days old')

        for user in users:
            await self.add_to_mod_log(user, ctx.author, 'Deleted messages', 'None')

    @commands.command()
    @commands.has_permissions(administrator=True)
    async def addmuterole(self, ctx):
        """Creates or updates the timeout role used by the bot"""
        query = 'SELECT mute_role_id FROM mod_values'
        async with self.bot.db.execute(query) as cursor:
                mute_role_id = await cursor.fetchall()

        if mute_role_id == [] or mute_role_id[0][0] == None:
            mute_role = await ctx.guild.create_role(name='Timeout')

            for channel in ctx.guild.channels:
                await channel.set_permissions(mute_role, send_messages=False, add_reactions=False)

            query = 'UPDATE mod_values SET mute_role_id = ?'
            await self.update_mod_values(query, [mute_role.id])

            await ctx.send('Mute role created')
        else:
            mute_role = ctx.guild.get_role(mute_role_id[0][0])

            for channel in ctx.guild.channels:
                await channel.set_permissions(mute_role, send_messages=False, add_reactions=False)

            await ctx.send('Mute role updated')

    async def unmute_user(self, user_id, ctx=None):
        if not await self.get_mute_role():
            #TODO: Log this error in a way that the bot operator can see it
            return
        try:
            user = get(self.mute_role.guild.members, id=user_id)
            if user:
                await user.remove_roles(self.mute_role)

                query = 'DELETE FROM muted_users WHERE user_id = ?'
                await self.bot.db.execute(query, [user_id])
                await self.bot.db.commit()

                if ctx:
                    await ctx.send(f'{user.display_name} unmuted')
        except (discord.Forbidden, discord.NotFound):
            print(f'Failed to unmute user: {user_id}')

    @tasks.loop(seconds=5)
    async def check_mute(self):
        query = 'SELECT * FROM muted_users'
        async with self.bot.db.execute(query) as cursor:
            entries = await cursor.fetchall()
            current_time = datetime.datetime.utcnow()

            for entry in entries:
                if current_time >= datetime.datetime.strptime(entry[MUTED_USERS_END_TIME], '%Y-%m-%d %H:%M:%S.%f'):
                    await self.unmute_user(entry[MUTED_USERS_USER_ID])

    @commands.command()
    @commands.has_permissions(administrator=True)
    async def unmute(self, ctx, users: commands.Greedy[commands.MemberConverter] = []):
        """Unmutes the users"""
        for user in users:
            await self.unmute_user(user.id, ctx)

    @commands.Cog.listener()
    async def on_member_join(self, member):
        query = 'SELECT EXISTS(SELECT 1 FROM muted_users WHERE user_id = ?)'
        async with self.bot.db.execute(query, [member.id]) as cursor:
            is_user_muted = await cursor.fetchall()
            is_user_muted = is_user_muted[0][0]

            if is_user_muted:
                if not await self.get_mute_role():
                    return
                await member.add_roles(self.mute_role)
                await self.message_user(member, f'You left while you were muted, and have been muted again')

    @commands.command()
    @commands.has_permissions(administrator=True)
    async def mute(self, ctx, users: commands.Greedy[commands.MemberConverter], duration_value: int, duration_type: str, *, reason: str = None):
        """Mutes the users for the given duration"""
        if users == []:
            await ctx.send('```.mute [users] [reason] [durationValue] [durationType]\n\n'
                            'Mutes the user for [duration_value] [duration_type].\n'
                            'Example: Mute @stupid "dummy" 1 year'
                            '```')
            return

        duration_types = {'seconds': 1, 'minutes': 60, 'hours': 3600, 'days': 3600*24, 'years': 3600*24*365}
        if not duration_type.lower() in duration_types.keys():
            await ctx.send(f'Please enter a valid durationType (seconds, minutes, days, years')
            return

        current_time = datetime.datetime.utcnow()
        mute_end_time = current_time + datetime.timedelta(seconds=duration_value*duration_types[duration_type.lower()])

        if not await self.get_mute_role():
            await ctx.send(f'Please use `.setmuterole` to let the bot know which role to add on mute')
            return

        query = 'INSERT INTO muted_users VALUES (?, ?, ?)'
        for user in users:
            await self.bot.db.execute(query, [user.id, mute_end_time, 0])
            await self.bot.db.commit()

            await user.add_roles(self.mute_role)
            await ctx.send(f'{user.mention} https://tenor.com/view/chazz-yu-gi-oh-shut-up-quiet-anime-gif-16356099')

            await self.add_to_mod_log(user, ctx.author, 'Muted', f'{reason} - {duration_value} {duration_type}')

            await self.message_user(user, f'You have been muted for {duration_value} {duration_type}\nReason: {reason}\n'
                                                'https://tenor.com/view/chazz-yu-gi-oh-shut-up-quiet-anime-gif-16356099')

    @commands.command()
    @commands.has_permissions(ban_members=True)
    async def ban(self, ctx, users: commands.Greedy[commands.MemberConverter], delete_message_days: typing.Optional[int] = 0, *, reason: str = None):
        """Bans the given user"""
        if users != []:
            for user in users:
                # Making sure that the user does not ban themself
                if ctx.author == users:
                    await ctx.send('Don\'t ban yourself!!! >.<')
                else:
                    # Checks if the user you want to ban is someone you should ban
                    # Don't know if I need to check this but I will anyway
                    if user.guild_permissions.ban_members:
                        await ctx.send('Don\'t ban other cool people!!! >.<')
                    else:
                        #Needs to attempt to send the message before the user is banned
                        await self.message_user(user, f'You have been banned.\nReason: {reason}')

                        # Bans the given user
                        await ctx.send(f'{user.mention}, get bapped for {reason}!')
                        await user.ban(delete_message_days=delete_message_days, reason=reason)

                        await self.add_to_mod_log(user, ctx.author, 'Banned', reason)
        else:
            await ctx.send(f'No users found')

    @commands.command(aliases=['setstatus'])
    @commands.has_permissions(administrator=True)
    async def setactivity(self, ctx, *, status: str = None):
        """Sets the bot's activity"""
        if status == None:
            activity = None
        else:
            activity = discord.Game(name=status)

        await self.bot.change_presence(activity=activity)
        await ctx.send('Status successfully changed')

    @commands.command()
    @commands.has_permissions(administrator=True)
    async def shutdown(self, ctx):
        """Shuts down the bot"""
        await ctx.send(f'Bai bai :wave:')
        await self.bot.logout()

def setup(bot):
    bot.add_cog(Mod(bot))
