# ? rename this to addReactionsToImages
import re
from discord.ext import commands


class ImageReact(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

        # Reading the 'reaction_channels' table
        self.bot.loop.create_task(self.initialize_reaction_channels())

        self.image_extensions = ['.png', '.jpg', '.jpeg', '.bmp', '.gif']

    async def initialize_reaction_channels(self):
        query = 'CREATE TABLE IF NOT EXISTS reaction_channels (' \
                'channel_id INTEGER PRIMARY KEY, ' \
                'emoji BLOB)'
        await self.bot.db.execute(query)
        await self.bot.db.commit()

        query = 'SELECT channel_id, emoji FROM reaction_channels'
        async with self.bot.db.execute(query) as cursor:
            self.reaction_channels = {key: value for (key, value) in await cursor.fetchall()}

    def has_url(self, message):
        pattern = r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\), ]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
        return re.findall(pattern, message.content)

    def image_check(self, message):
        """Checks if the message has an image uploaded, or it has one embeded"""
        return message.attachments or message.embeds or self.has_url(message)

    @commands.Cog.listener()
    async def on_message(self, message):
        # Ignores the bot's messages
        if message.author == self.bot.user:
            return

        if message.channel.id in self.reaction_channels:
            if self.image_check(message):
                for emoji in self.reaction_channels[message.channel.id].split(' '):
                    await message.add_reaction(emoji)

    @commands.group()
    @commands.has_permissions(manage_channels=True)
    async def imagereact(self, ctx):
        """Specifies channels that, when an image is posted the bot will react with the given emoji"""
        if ctx.invoked_subcommand is None:
            await ctx.send_help(ctx.command)

    @imagereact.command()
    async def watch(self, ctx, channel_id: int, *, emoji):
        """Watches the specified channel for any images, and reacts with all emoji given"""
        # If the given channel is not already being reacted to
        if channel_id not in self.reaction_channels:
            reaction_channel = ctx.guild.get_channel(channel_id)

            # If the channel exists
            if reaction_channel:
                query = 'INSERT INTO reaction_channels VALUES(?, ?)'
                await self.bot.db.execute(query, [channel_id, emoji])
                await self.bot.db.commit()
                self.reaction_channels[reaction_channel.id] = emoji

                await ctx.send(f'Now reacting to all images in the given channel with the {emoji} emoji')
            else:
                await ctx.send('Could not find the given channel')
                return
        else:
            # Replaces the current emoji with the new ones
            query = 'UPDATE reaction_channels SET ' \
                            'emoji = ? ' \
                    'WHERE channel_id = ?'
            await self.bot.db.execute(query, [emoji, channel_id])
            await self.bot.db.commit()
            self.reaction_channels[channel_id] = emoji
            await ctx.send(f'I am already watching that channel, switching to react with {emoji}')

    @imagereact.command()
    async def unwatch(self, ctx, channel_id: int):
        """Unwatches the specified channel"""
        # If the given channel is being reacted to
        if channel_id in self.reaction_channels:
            # Removes the channel
            query = 'DELETE FROM reaction_channels ' \
                    'WHERE channel_id = ?'
            await self.bot.db.execute(query, [channel_id])
            await self.bot.db.commit()
            del self.reaction_channels[channel_id]
            await ctx.send('I am no longer watching that channel')
        else:
            await ctx.send('I am not watching that channel')

def setup(bot):
    bot.add_cog(ImageReact(bot))
