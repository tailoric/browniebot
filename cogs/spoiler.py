from aiohttp import ClientSession
from discord.ext import commands

import discord
import io
import typing

class NotUrlError(commands.CheckFailure):
    pass

class SimpleUrlArg(commands.Converter):
    async def convert(self, ctx, argument):
        if not argument.startswith(("http", "https")):
            raise NotUrlError("First argument was not an URL.")
        else:
            return argument

class SpoilerCheck(commands.Cog):

    def cog_unload(self):
        self.bot.loop.create_task(self.session.close())

    def __init__(self, bot: commands.Bot):
        self.bot : commands.Bot = bot
        self.session : ClientSession = ClientSession()


    missing_source : str = ("Please provide what this is spoiling"
            " or give a NSFW or Content Warning after the link."
            " (or in the input field of the file upload)")

    @commands.command(name="spoiler")
    @commands.guild_only()
    async def spoiler(self, ctx, link: typing.Optional[SimpleUrlArg], * , source):
        """
        command for reuploading an image or file spoiler tagged (for mobile users)
        """
        filename = None
        if not link and ctx.message.attachments:
            attachment: discord.Attachment = ctx.message.attachments[0]
            link = attachment.url
            filename = attachment.filename
        if ctx.guild.me.guild_permissions.manage_messages:
            await ctx.message.delete()
        if not link and not ctx.message.attachments:
            return await ctx.send("There was no file attached to this message")
        async with self.session.get(url=link) as resp:
            resp.raise_for_status()
            if not filename:
                filename = link.rsplit("/")[-1]
            byio = io.BytesIO(await resp.read())           
            await ctx.send(content=f"File spoiler tagged for user {ctx.author.mention}\nSpoiler/Content Warning: **[{source}]**",
                    file=discord.File(fp=byio, filename=filename, spoiler=True),
                    allowed_mentions=discord.AllowedMentions.none())



    @spoiler.error
    async def spoiler_error(self, ctx : commands.Context, error):
        if ctx.guild and ctx.guild.me.guild_permissions.manage_messages:
            await ctx.message.delete()
            await ctx.send(self.missing_source)
    

def setup(bot):
    bot.add_cog(SpoilerCheck(bot))
