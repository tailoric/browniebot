import datetime
import discord
from discord.ext import commands, tasks

# Database index legend
ALLOWED_ROLES_ROLE_ID = 0
ALLOWED_ROLES_ROLE_DESCRIPTION = 1
ALLOWED_ROLES_ROLE_REMOVABLE = 2

ALLOWED_ROLES_USER_COUNT_ROLE_ID = 0
ALLOWED_ROLES_USER_COUNT_USER_COUNT = 1
ALLOWED_ROLES_USER_COUNT_JOINED_COUNT = 2

ROLE_OUTPUT_CHANNEL_ID = 0
ROLE_OUTPUT_MESSAGE = 1

RULES_ROLES_ROLE_ID = 0
RULES_ROLES_RULES_CHANNEL_ID = 1

class Roles(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

        self.bot.loop.create_task(self.initialize_allowed_roles())

    async def initialize_allowed_roles(self):
        query = 'CREATE TABLE IF NOT EXISTS allowed_roles (' \
                'role_id INTEGER PRIMARY KEY, ' \
                'role_description TEXT, ' \
                'role_removable INTEGER)'
        await self.bot.db.execute(query)

        query = 'CREATE TABLE IF NOT EXISTS allowed_roles_user_count (' \
                'role_id INTEGER PRIMARY KEY, ' \
                'user_count INTEGER, ' \
                'joined_count INTEGER, ' \
                'FOREIGN KEY(role_id) REFERENCES allowed_roles(role_id))'
        await self.bot.db.execute(query)

        query = 'CREATE TABLE IF NOT EXISTS role_output (' \
                'role_id INTEGER PRIMARY KEY, ' \
                'channel_id INTEGER, ' \
                'message TEXT, ' \
                'FOREIGN KEY(role_id) REFERENCES allowed_roles(role_id))'
        await self.bot.db.execute(query)

        query = 'CREATE TABLE IF NOT EXISTS rules_roles (' \
                'role_id INTEGER, ' \
                'rules_channel_id INTEGER, ' \
                'PRIMARY KEY (role_id, rules_channel_id))'
        await self.bot.db.execute(query)
        await self.bot.db.commit()

        self.reset_limit.start()

    @commands.group()
    async def roles(self, ctx):
        """Lets users assign and removes alowed roles from themselves"""
        if ctx.invoked_subcommand is None:
            await ctx.send_help(ctx.command)


    @roles.command()
    async def list(self, ctx):
        """Lists all assignable roles on the server"""
        query = 'SELECT role_id, role_description FROM allowed_roles'

        allowed_roles = []

        async with self.bot.db.execute(query) as cursor:
            allowed_roles = await cursor.fetchall()

        roles = []

        for allowed_role in allowed_roles:
            curr_role = ctx.guild.get_role(allowed_role[ALLOWED_ROLES_ROLE_ID])

            if curr_role:
                roles.append((curr_role.name, allowed_role[ALLOWED_ROLES_ROLE_DESCRIPTION]))

        if len(roles) == 0:
            await ctx.send('No roles are assignable...')
            return

        # Formatting the text
        list_of_roles = '```\n'
        for (role, _) in roles:
            list_of_roles += f'{role}\n'
        list_of_roles += '```'
        await ctx.send(f'The assignable roles are:\n{list_of_roles}')

    def get_role_from_roles(self, roles, role_name):
        """Getting the role if it exists"""
        found_roles = [role for role in roles if role.name.lower() == role_name.lower()]

        if found_roles != []:
            # Getting the first role
            return found_roles[0]
        else:
            return None

    @roles.command()
    async def info(self, ctx, role_name: str = None):
        """Displays information about the given role, if it is allowed"""
        if role_name:
            role = self.get_role_from_roles(ctx.guild.roles, role_name)
            if role:
                query = 'SELECT role_description FROM allowed_roles WHERE role_id = ?'
                role_description = []

                async with self.bot.db.execute(query, [role.id]) as cursor:
                    role_description = await cursor.fetchall()

                if role_description != []:
                    # This implies that the role is allowed
                    role_embed = discord.Embed(colour=role.colour, title=role.name, description=role_description[0][0])
                    await ctx.send(embed=role_embed)
                    return
            clean_role_name = await self.bot.clean_converter.convert(ctx, role_name)
            await ctx.send(f'Role \'{clean_role_name}\' does not exist, or it is not allowed')
        else:
            await ctx.send('```.roleinfo "[role]"\n\nDisplays information '
                           'about the given role, if it is allowed.```')

    @roles.command()
    @commands.has_permissions(manage_roles=True)
    async def create(self, ctx, role_name: str = None, description: str = ''):
        """Creates a role with the given name"""
        if role_name:
            clean_role_name = await self.bot.clean_converter.convert(ctx, role_name)
            # Checks if a role with the given name already exists
            if not any(role.name == role_name for role in ctx.guild.roles):
                # Creates the role
                new_role = await ctx.guild.create_role(
                    name=role_name,
                    permissions=discord.Permissions(0),
                    colour=discord.Colour(0),
                    hoist=False,
                    mentionable=False
                )

                query = 'INSERT INTO allowed_roles VALUES (?, ?, 1)'
                await self.bot.db.execute(query, [new_role.id, description])
                await self.bot.db.commit()

                await ctx.send(f'Role \'{clean_role_name}\' successfully created')
            else:
                await ctx.send(f'Role \'{clean_role_name}\' already exists')
        else:
            await ctx.send('```.createrole "[role_name]" "[description]"\n\nCreates a role '
                            'with the given name and description.\n'
                            'The name of the role is case sensitive.```')

    async def get_role_status(self, query, role):
        """
            Queries put into this function should always want the first result's first value.
            Only used to remove this nonsense from the functions themselves.
        """
        role_status = None
        async with self.bot.db.execute(query, [role.id]) as cursor:
                        role_status = await cursor.fetchall()
                        try:
                            role_status = role_status[0][0]
                        except IndexError:
                            pass

        return role_status

    @roles.command()
    @commands.has_permissions(manage_roles=True)
    async def delete(self, ctx, role_name: str = None):
        """Deletes the role with the given name, if it is allowed"""
        if role_name:
            role = self.get_role_from_roles(ctx.guild.roles, role_name)
            if role:
                query = 'SELECT EXISTS(SELECT 1 FROM allowed_roles WHERE role_id = ?)'
                role_found = await self.get_role_status(query, role)

                if role_found:
                    # Removing the role
                    query = 'DELETE FROM allowed_roles ' \
                            'WHERE role_id = ?'

                    await self.bot.db.execute(query, [role.id])
                    await self.bot.db.commit()
                    await role.delete()
                    await ctx.send(f'Role \'{role.name}\' successfully deleted')

                    return
            clean_role_name = await self.bot.clean_converter.convert(ctx, role_name)
            await ctx.send(f'Role \'{clean_role_name}\' does not exist, or it is not allowed')
        else:
            await ctx.send('```.deleterole "[roleName]"\n\nDeletes the role '
                            'with the given name, if it is allowed.\n'
                            'The name of the role is case sensitive.```')

    @roles.command()
    @commands.has_permissions(manage_roles=True)
    async def add(self, ctx, role_name: str = None, description: str = ''):
        """Adds the given role to the allowed roles
            Will change the description if the role already exists"""
        if role_name:
            # Checks if a role with the given name exists
            role = self.get_role_from_roles(ctx.guild.roles, role_name)
            if role:
                query = 'REPLACE INTO allowed_roles (role_id, role_description, role_removable) VALUES (?, ?, 1)'
                await self.bot.db.execute(query, [role.id, description])
                await self.bot.db.commit()

                await ctx.send(f'Role \'{role.name}\' successfully added to allowed roles or description updated')
                return
            clean_role_name = await self.bot.clean_converter.convert(ctx, role_name)
            await ctx.send(f'Role \'{clean_role_name}\' does not exist')
        else:
            await ctx.send('```.addrole "[roleName]" "[description]"\n\n'
                            'Adds the given role to the allowed roles. '
                            'Will update the description if the role is already allowed.```')

    @roles.command()
    @commands.has_permissions(manage_roles=True)
    async def remove(self, ctx, role_name: str = None):
        """Removes the given role from the allowed roles"""
        if role_name:
            # Checks if a role with the given name exists
            role = self.get_role_from_roles(ctx.guild.roles, role_name)
            if role:
                query = 'DELETE FROM allowed_roles ' \
                        'WHERE role_id = ?'

                output = await self.bot.db.execute(query, [role.id])
                await self.bot.db.commit()

                if output.rowcount:
                    await ctx.send(f'Role \'{role.name}\' successfully removed from allowed roles')
                else:
                    await ctx.send(f'Role \'{role.name}\' not allowed to begin with')
                return
            clean_role_name = await self.bot.clean_converter.convert(ctx, role_name)
            await ctx.send(f'Role \'{clean_role_name}\' does not exist')
        else:
            await ctx.send('```.removerole "[roleName]"\n\n'
                            'Removes the given role from the allowed roles.```')

    @roles.command()
    @commands.has_permissions(manage_roles=True)
    async def announce(self, ctx, role_name: str = None, channel_id: int = None, message: str = None):
        """Adds the custom announcement message when the given role is given"""
        if role_name:
            role = self.get_role_from_roles(ctx.guild.roles, role_name)
            if role:
                channel = self.bot.get_channel(channel_id)
                if not channel:
                    await ctx.send(f'The channel with if {channel_id} was not found.')
                    return

                query = 'REPLACE INTO role_output VALUES (?, ?, ?)'
                await self.bot.db.execute(query, [role.id, channel_id, message])
                await self.bot.db.commit()
                await ctx.send(f'Role \'{role.name}\' successfully added to announced roles or updated')
                return

            clean_role_name = await self.bot.clean_converter.convert(ctx, role_name)
            await ctx.send(f'Role \'{clean_role_name}\' does not exist')
        await ctx.send('```.announcerole "[roleName]" [channel_id] "[message]"\n\n'
            'Says "[message], [user]" in [channel_id] when the given role is given.```')

    @roles.command()
    @commands.has_permissions(manage_roles=True)
    async def surpress(self, ctx, role_name: str = None):
        """Stops announcing when the given role is given"""
        if role_name:
            role = self.get_role_from_roles(ctx.guild.roles, role_name)
            if role:
                query = 'SELECT EXISTS(SELECT 1 FROM role_output WHERE role_id = ?)'
                role_found = await self.get_role_status(query, role)

                if role_found:
                    query = 'DELETE FROM role_output ' \
                            'WHERE role_id = ?'

                    await self.bot.db.execute(query, [role.id])
                    await self.bot.db.commit()
                    await ctx.send(f'Role \'{role.name}\' will no longer be announced')
                    return

            clean_role_name = await self.bot.clean_converter.convert(ctx, role_name)
            await ctx.send(f'Role \'{clean_role_name}\' does not exist')
        else:
            await ctx.send('```.surpressrole "[roleName]"\n\n'
                'Stops announcing the given role. See .announcerole for what that entails.```')

    @roles.command(aliases=['toggle'])
    @commands.has_permissions(manage_roles=True)
    async def toggleroleremoval(self, ctx, role_name: str = None):
        """Makes the role removable if it was not, and vice versa"""
        if role_name:
            role = self.get_role_from_roles(ctx.guild.roles, role_name)
            if role:
                query = 'SELECT role_removable FROM allowed_roles WHERE role_id = ?'
                role_status = await self.get_role_status(query, role)

                # The role is not allowed
                if role_status != None:
                    # Flipping the state
                    role_status = 1 - role_status

                    query = 'UPDATE allowed_roles ' \
                            'SET role_removable = ? ' \
                            'WHERE role_id = ?'

                    await self.bot.db.execute(query, [role_status, role.id])
                    await self.bot.db.commit()

                    await ctx.send(f'Role \'{role.name}\' is now {"removable" if role_status else "not removable"}')
                    return
            clean_role_name = await self.bot.clean_converter.convert(ctx, role_name)
            await ctx.send(f'Role \'{clean_role_name}\' does not exist, or it is not allowed')
        else:
            await ctx.send('```.toggleroleremoval "[roleName]"\n\n'
                            'Makes the role removable if it was not, and vice versa.```')

    @roles.command()
    @commands.has_permissions(manage_roles=True)
    async def mention(self, ctx, role_name: str = None):
        """Makes the role mentionable if it isn't, and then mentions it"""
        if role_name:
            role = self.get_role_from_roles(ctx.guild.roles, role_name)
            if role:
                # Gets the mentionable status of the role
                mention_status = role.mentionable
                try:
                    # Makes the role mentionable
                    if not mention_status:
                        await role.edit(mentionable=True)

                    # Mentions the role
                    await ctx.send(role.mention)

                    # Makes the role not mentionable
                    if not mention_status:
                        await role.edit(mentionable=mention_status)
                except Exception as e:
                    await ctx.send(e)
                    await ctx.send(f'I can\'t mention the {role.name} role')
                return
            clean_role_name = await self.bot.clean_converter.convert(ctx, role_name)
            await ctx.send(f'Role \'{clean_role_name}\' does not exist, or it is not allowed')
        else:
            await ctx.send('```.mention "[roleName]"\n\n'
                            'Mentions the given role if it is allowed```')

    async def announce_role(self, role_id, display_name):
        query = 'SELECT channel_id, message FROM role_output WHERE role_id = ?'

        async with self.bot.db.execute(query, [role_id]) as cursor:
            output = await cursor.fetchall()

            if output:
                channel = self.bot.get_channel(output[0][ROLE_OUTPUT_CHANNEL_ID])
                await channel.send(f'{output[0][ROLE_OUTPUT_MESSAGE]}, {display_name}!')

    @tasks.loop(hours=24)
    async def reset_limit(self):
        """Resets the joined_count for all roles on a daily basis"""
        query = 'UPDATE allowed_roles_user_count SET joined_count = 0'
        await self.bot.db.execute(query)
        await self.bot.db.commit()

    @roles.command()
    @commands.has_permissions(manage_roles=True)
    async def viewlimit(self, ctx):
        """Displays the number of people that can join the role each day, and how many have joined already"""
        query = 'SELECT * FROM allowed_roles_user_count'
        async with self.bot.db.execute(query) as cursor:
                allowed_roles_user_count = await cursor.fetchall()
        if len(allowed_roles_user_count) == 0:
            await ctx.send('No roles are being limited right now')
            return
        output = '```\n'
        for entry in allowed_roles_user_count:
            role = ctx.guild.get_role(entry[ALLOWED_ROLES_USER_COUNT_ROLE_ID])
            remaining_count = entry[ALLOWED_ROLES_USER_COUNT_USER_COUNT] - entry[ALLOWED_ROLES_USER_COUNT_JOINED_COUNT]

            next_reset = self.reset_limit.next_iteration - datetime.datetime.now(datetime.timezone.utc)
            next_reset = round(next_reset.seconds / 3600)

            output += f'{role.name} allows {entry[ALLOWED_ROLES_USER_COUNT_USER_COUNT]} user, {remaining_count} remaining\n' \
                        f'The next refresh is in {next_reset} hours'

        output += '```'
        await ctx.send(output)

    @roles.command()
    @commands.has_permissions(manage_roles=True)
    async def unlimitrole(self, ctx, role_name: str = None):
        """Removes the limit on the number of people who can get the given role in a day"""
        if role_name:
            role = self.get_role_from_roles(ctx.guild.roles, role_name)
            if role:
                query = 'DELETE FROM allowed_roles_user_count WHERE role_id = ?'

                await self.bot.db.execute(query, [role.id])
                await self.bot.db.commit()
                await ctx.send(f'Role \'{role.name}\' no longer limited')
                return
            clean_role_name = await self.bot.clean_converter.convert(ctx, role_name)
            await ctx.send(f'Role \'{clean_role_name}\' does not exist')
        else:
            await ctx.send('```.unlimitrole [role_ame]\n\nRemoves the limit on the number of users '
                            'who can get the given role.```')

    @roles.command()
    @commands.has_permissions(manage_roles=True)
    async def limitrole(self, ctx, role_name: str = None, user_count = 20):
        """Limits the number of people who can get the given role in a day"""
        if role_name:
            role = self.get_role_from_roles(ctx.guild.roles, role_name)
            if role:
                query = 'REPLACE INTO allowed_roles_user_count VALUES (?, ?, ?)'

                await self.bot.db.execute(query, [role.id, user_count, 0])
                await self.bot.db.commit()
                await ctx.send(f'Role \'{role.name}\' now limited to {user_count} users')
                return
            clean_role_name = await self.bot.clean_converter.convert(ctx, role_name)
            await ctx.send(f'Role \'{clean_role_name}\' does not exist')
        else:
            await ctx.send('```.limitrole [role_ame] [user_count]\n\nLimits the number of users '
                            'who can get the given role.```')

    @roles.command()
    @commands.has_permissions(manage_roles=True)
    async def unsetrules(self, ctx, *, role_name: str = None):
        """Unsets the given role to only be obtainable in the set channel"""
        if role_name:
            role = self.get_role_from_roles(ctx.guild.roles, role_name)
            if role:
                query = 'DELETE FROM rules_roles where role_id = ?'
                await self.bot.db.execute(query, [role.id])
                await self.bot.db.commit()
                await ctx.send(f'Role \'{role.name}\' is now obtainable in any channel')
                return
            clean_role_name = await self.bot.clean_converter.convert(ctx, role_name)
            await ctx.send(f'Role \'{clean_role_name}\' does not exist')
        else:
            await ctx.send('```.unsetrulesrole [role_name]\n\nUnsets the given role '
                            'to only be obtainable in the set channel')

    @commands.command()
    @commands.has_permissions(manage_roles=True)
    async def setrules(self, ctx, role_name: str = None, channel_id: int = None):
        """Sets the given role to only be obtainable in the given channel"""
        if role_name:
            role = self.get_role_from_roles(ctx.guild.roles, role_name)
            if role:
                channel = self.bot.get_channel(channel_id)
                if not channel:
                    await ctx.send(f'The channel with if {channel_id} was not found.')
                    return

                query = 'REPLACE INTO rules_roles VALUES (?, ?)'
                await self.bot.db.execute(query, [role.id, channel_id])
                await self.bot.db.commit()
                await ctx.send(f'Role \'{role.name}\' is now the only obtainable role in #{channel}')
                return
            clean_role_name = await self.bot.clean_converter.convert(ctx, role_name)
            await ctx.send(f'Role \'{clean_role_name}\' does not exist')
        else:
            await ctx.send('```.setrulesrole [role_name] [channel_id]\n\nSets the given role '
                            'to only be obtainable in the given channel')

    async def give_user_role(self, ctx, role_name: str):
        """Assigns the user the given role, if it is allowed"""
        if role_name:
            role = self.get_role_from_roles(ctx.guild.roles, role_name)
            if role:
                query = 'SELECT EXISTS(SELECT 1 FROM allowed_roles WHERE role_id = ?)'
                role_found = await self.get_role_status(query, role)

                if role_found:
                    query = 'SELECT role_id FROM rules_roles WHERE rules_channel_id = ?'
                    async with self.bot.db.execute(query, [ctx.channel.id]) as cursor:
                        rules_channel_id = await cursor.fetchall()

                    if rules_channel_id != []:
                        rules_channel_id = rules_channel_id[0][0]

                        if role.id != rules_channel_id:
                            await ctx.send('That is not the role you should be getting here')
                            return

                    query = 'SELECT * FROM allowed_roles_user_count WHERE role_id = ?'
                    async with self.bot.db.execute(query, [role.id]) as cursor:
                        role_status = await cursor.fetchall()

                    # If the daily limit has been reached
                    if role_status != []:
                        if role_status[0][ALLOWED_ROLES_USER_COUNT_JOINED_COUNT] >= role_status[0][ALLOWED_ROLES_USER_COUNT_USER_COUNT]:
                            next_reset = self.reset_limit.next_iteration - datetime.datetime.now(datetime.timezone.utc)
                            next_reset = round(next_reset.seconds / 3600)
                            await ctx.send(f'Sorry, the max number of users have joined for today. The next reset is in {next_reset} hours')
                            return
                        query = 'UPDATE allowed_roles_user_count SET joined_count = joined_count + 1 WHERE role_id = ?'
                        await self.bot.db.execute(query, [role.id])
                        await self.bot.db.commit()

                    # Checks if the user already has that role
                    if role not in ctx.author.roles:
                        try:
                            await ctx.author.add_roles(role)
                            await ctx.send(f'I\'ve given you the \'{role.name}\' role')
                            await self.announce_role(role.id, ctx.author.display_name)
                        except Exception as e:
                            print(e)
                            await ctx.send(f'I can\'t assign the \'{role.name}\' role')
                    else:
                        await ctx.send(f'You already have the \'{role.name}\' role')
                    return
            clean_role_name = await self.bot.clean_converter.convert(ctx, role_name)
            await ctx.send(f'Role \'{clean_role_name}\' does not exist, or it is not allowed')
        else:
            await ctx.send('```.iam [roleName]\n\nAssigns the user '
                           'the given role, if it is allowed.```')

    @commands.command()
    async def iam(self, ctx, *, role_name: str = None):
        """Assigns the user the given role, if it is allowed"""
        await self.give_user_role(ctx, role_name)

    @roles.command(name='iam')
    async def iam_group(self, ctx, *, role_name: str = None):
        """Assigns the user the given role, if it is allowed"""
        await self.give_user_role(ctx, role_name)

    async def remove_user_role(self, ctx, role_name: str):
        """Removes the user the given role, if it is allowed"""
        if role_name:
            role = self.get_role_from_roles(ctx.guild.roles, role_name)
            if role:
                query = 'SELECT EXISTS(SELECT 1 FROM allowed_roles WHERE role_id = ? AND role_removable = 1)'
                role_found = await self.get_role_status(query, role)

                if role_found:
                    # Checks if the user already has that role
                    if role in ctx.author.roles:
                        try:
                            await ctx.author.remove_roles(role)
                            await ctx.send(f'I\'ve removed the \'{role.name}\' role from you')
                        except Exception as e:
                            print(e)
                            await ctx.send(f'I can\'t remove the \'{role.name}\' role')
                    else:
                        await ctx.send(f'You don\'t have the \'{role.name}\' role')
                    return
            clean_role_name = await self.bot.clean_converter.convert(ctx, role_name)
            await ctx.send(f'Role \'{clean_role_name}\' does not exist, or it is not allowed')
        else:
            await ctx.send('```.iamnot [roleName]\n\nRemoves the given role '
                           'from the user, if it is allowed.```')
    @commands.command()
    async def iamnot(self, ctx, *, role_name: str = None):
        """Assigns the user the given role, if it is allowed"""
        await self.remove_user_role(ctx, role_name)

    @roles.command(name='iamnot')
    async def iam_group(self, ctx, *, role_name: str = None):
        """Assigns the user the given role, if it is allowed"""
        await self.remove_user_role(ctx, role_name)

def setup(bot):
    bot.add_cog(Roles(bot))
