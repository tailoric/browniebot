import discord
from discord.ext import commands


class RemoveTenor(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

        # Reading the 'remove_tenor_channels' table
        self.bot.loop.create_task(self.initialize_remove_tenor_channels())

    async def initialize_remove_tenor_channels(self):
        query = 'CREATE TABLE IF NOT EXISTS remove_tenor_channels (' \
                'channel_id INTEGER PRIMARY KEY)'
        await self.bot.db.execute(query)
        await self.bot.db.commit()

        query = 'SELECT channel_id FROM remove_tenor_channels'
        async with self.bot.db.execute(query) as cursor:
            self.remove_tenor_channels = [channel[0] for channel in await cursor.fetchall()]

    @commands.Cog.listener()
    async def on_message(self, message):
        # Ignores the bot's messages
        if message.author == self.bot.user:
            return

        if message.channel.id in self.remove_tenor_channels:
            if 'https://tenor.com/view' in message.content:
                await message.delete()

                await message.channel.send('No tenor links in this channel please')

                mod = self.bot.get_cog('Mod')
                if mod is not None:
                    await mod.add_to_mod_log(message.author, self.bot.user, 'Warning', 'Posted tenor link in ' + message.channel.mention)

    @commands.group()
    @commands.has_permissions(manage_messages=True)
    async def tenor(self, ctx):
        """Specifies channels to delete tenor links from"""
        if ctx.invoked_subcommand is None:
            await ctx.send_help(ctx.command)

    @tenor.command()
    async def remove(self, ctx, channel_id: int):
        """Removes tenor links posted in the specified chanel"""
        remove_tenor_channel = ctx.guild.get_channel(channel_id)

        # If the channel exists
        if remove_tenor_channel:
            query = 'INSERT INTO remove_tenor_channels VALUES(?)'
            await self.bot.db.execute(query, [channel_id])
            await self.bot.db.commit()
            self.remove_tenor_channels.append(channel_id)

            await ctx.send(f'Now removing tenor links from the given channel')
        else:
            await ctx.send('Could not find the given channel')
            return

    @tenor.command()
    async def allow(self, ctx, channel_id: int):
        """Stops removing tenor links from the specified channel"""
        # If the given channel is being reacted to
        if channel_id in self.remove_tenor_channels:
            # Removes the channel
            query = 'DELETE FROM remove_tenor_channels ' \
                    'WHERE channel_id = ?'
            await self.bot.db.execute(query, [channel_id])
            await self.bot.db.commit()
            self.remove_tenor_channels.remove(channel_id)
            await ctx.send('I am no longer removing tenor links from that channel')
        else:
            await ctx.send('I am not removing tenor links from that channel')

def setup(bot):
    bot.add_cog(RemoveTenor(bot))
