import textwrap
from discord.ext import commands


class Help():
    def __init__(self, bot):
        self.bot = bot

    # New help command
    @commands.command(pass_context=True)
    async def help(self, context):
        """Sends the list of functions and what they do to the user that typed it"""
        await self.bot.send_message(context.message.author, textwrap.dedent(
            # ? move these descriptions to variables contained inside the command
            f"""
            ```
            Basic Commands:
                ping                pong!
                echo                Echoes a message
                ninja               Like echo, but deletes the message that issued the command
                figlet              Echoes a message with figlet
                roll                Rolls dice using NdN format, allows for multiple dice rolls using '+' as a seperator
                userinfo            Prints out info relating to the user that typed this message, or whoever was mentioned in this message
            Music:
                play                Plays the song that is typed in, or adds it to queue
                playing             Displays information on the song currently playing, and lists the queue
                skip                Skips the currently playing song
                resume              Resumes playback of the song
                pause               Pauses the playing of the song
            Misc:
                iloveyou            Expresses my love the user who typed this command
            Roles:
                roles               Print all roles on this server
                roleinfo            Prints the user which have the given role
                createrole          Creates a role with the given name
                deleterole          Deletes the role with the given name
                iam                 Assigns the user the given role, if able
                iamnot              Removes the given role from the user, if able
                mention             Mentions the given role, if able
            Mod:
                delete              Deletes messages equal to the number following this command. Only allows 1-99.
            ```"""
        ))


def setup(bot):
    bot.add_cog(Help(bot))
